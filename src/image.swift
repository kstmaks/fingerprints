import Cocoa

extension NSImage {
    func bitmapRepresentation() -> NSBitmapImageRep {
        let cgRef = self.CGImageForProposedRect(nil, context: nil, hints: nil)
        return NSBitmapImageRep(CGImage: cgRef!.takeRetainedValue())
    }
}

public class Image {

    public private(set) var width: NSInteger = 0
    public private(set) var height: NSInteger = 0
    public private(set) var data: [Double] = []

    private var nsImage: NSImage? = nil
    
    init?(fromFile path: String) {
        let nsImage = NSImage(contentsOfFile: path)
        if nsImage == nil { return nil }
        self.nsImage = nsImage

        let repr = NSBitmapImageRep(data: nsImage!.TIFFRepresentation!)!
        self.width = repr.pixelsWide
        self.height = repr.pixelsHigh
        self.data = []
        let bitmap = repr.bitmapData

        let doubles = map(0...self.height-1) { (y: Int) in
            map(0...self.width-1) { (x: Int) in
                Double((bitmap + (x + y * self.width) * 3).memory) / 255.0
            }
        }
        self.data = reduce(doubles, [], +)
    }

    init(data newData: [Double], width: Int, height: Int) {
        self.width  = width
        self.height = height
        self.data   = newData
    }

    public func genNSImage() -> NSImage {
        if self.nsImage != nil { return self.nsImage! }

        let repr = NSBitmapImageRep(
            bitmapDataPlanes: nil,
            pixelsWide:       width,
            pixelsHigh:       height,
            bitsPerSample:    8,
            samplesPerPixel:  3,
            hasAlpha:         false,
            isPlanar:         false,
            colorSpaceName:   NSCalibratedRGBColorSpace,
            bytesPerRow:      4 * width,
            bitsPerPixel:     24
            )

        for (i, value) in enumerate(data) {
            let x     = i % width
            let y     = (i - x) / width
            let fl    = CGFloat(max(min(value, 1.0), -1.0))
            let color = (fl < 0) 
                      ? NSColor(calibratedRed: -fl, green: 0.0, blue: 0.0, alpha: 1.0)
                      : NSColor(calibratedRed: fl, green: fl, blue: fl, alpha: 1.0)
            repr!.setColor(color, atX: x, y: y)
        }

        self.nsImage = NSImage(size: NSSize(width: width, height: height))
        self.nsImage?.addRepresentation(repr!)
        return self.nsImage!
    }

    public func at(xy: (Int, Int)) -> Double {
        let (x, y) = xy
        let lx = max(0, min(width-1, x))
        let ly = max(0, min(height-1, y))
        return self.data[ly * width + lx]
    }
}

public typealias Image2 = (Image, Image)
public typealias Image3 = (Image, Image, Image)
