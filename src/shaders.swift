import Cocoa

func sobelShader() -> Shader2 {
    let sobelXKernel: [[Double]] = [
        [-1, 0, 1],
        [-2, 0, 2],
        [-1, 0, 1]
    ]

    let sobelYKernel: [[Double]] = [
        [-1, -2, -1],
        [ 0,  0,  0],
        [ 1,  2,  1]
    ]

    return { (image, color, x, y) in
        var sx = 0.0
        var sy = 0.0

        for i in -1...1 {
            for j in -1...1 {
                let value = image.at((x+i, y+j))
                sx += value * sobelXKernel[i+1][j+1]
                sy += value * sobelYKernel[i+1][j+1]
            }
        }

        if sx != 0 && sy != 0 && sx == sy {
            sx += (Double(arc4random_uniform(3)) - 1) * 0.01
        }

        return (sx, sy)   
    }
}

func fixSobelShader(second: Image) -> Shader {
    return { (image, color, x, y) in
        let secondColor = second.at((x, y))
        if color != 0 && secondColor != 0 && color == secondColor {
            return color + (Double(arc4random_uniform(3)) - 1) * 0.01
        }
        return color
    }
}

func orientCohShader(#phy: Int, #sobel: Image2) -> Shader3 {
    let lophy = -phy/2
    let hiphy = phy/2
    let (sobelX, sobelY) = sobel

    return { (image, color, x, y) in
        if x % phy != hiphy || y % phy != hiphy {
            return (0.0, 0.0, 0.0)
        }

        var gsx = 0.0
        var gsy = 0.0
        var gxy = 0.0
        var gxx = 0.0
        var gyy = 0.0

        for i in lophy...hiphy {
            for j in lophy...hiphy {
                let gx = sobelX.at((x+i, y+j))
                let gy = sobelY.at((x+i, y+j))
                gxy += gx * gy
                gxx += gx * gx
                gyy += gy * gy
                gsx += gx * gx - gy * gy
                gsy += 2 * gx * gy
            }
        }

        var field = (-0.5 * atan2(gsy, gsx)) % (2 * PI)
        if field < 0 { field += PI }

        let gdiff = gxx - gyy
        let coh = sqrt(gdiff * gdiff + 4.0 * gxy * gxy) / (gxx + gyy)
        let length = sqrt(gsx*gsx + gsy*gsy)

        return (field, coh, length)
    }
}

func blockEstimationShader(#phy: Int, #orient: Image, #coherence: Image) -> Shader {
    return { (image, color, x, y) in
        let lophy = -phy/2
        let hiphy = phy/2

        let lx = x % phy
        let ly = y % phy
        if lx != hiphy || 
           ly != hiphy {
            return 0.0
        }

        if x - phy < 0 || 
           x + phy >= image.width || 
           y - phy < 0 || 
           y + phy >= image.height {
            return orient.at((x, y))
        }

        var cohs: [Double] = [0, 0, 0, 0]

        for di in 0...1 {
            for dj in 0...1 {
                cohs[0] += coherence.at((x + phy * (di - 1), y + phy * (dj - 1)))
                cohs[1] += coherence.at((x + phy * (di    ), y + phy * (dj - 1)))
                cohs[2] += coherence.at((x + phy * (di - 1), y + phy * (dj    )))
                cohs[3] += coherence.at((x + phy * (di    ), y + phy * (dj    )))
            }
        }
        for i in 0...3 {
            cohs[i] /= 4.0
        }

        var imax = 0
        var jmax = 0
        var max = -Double.infinity

        for dj in 0...1 {
            for di in 0...1 {
                let val = cohs[dj * 2 + di]
                if val > max {
                    imax = di
                    jmax = dj
                    max  = val
                }
            }
        }

        imax -= 1
        jmax -= 1

        var field = 0.0
        var fieldSet = false

        func averageAngle(a: Double, b: Double) -> Double {
            let diff = ((a - b + PI + TwoPI) % TwoPI) - PI
            return (TwoPI + b + (diff / 2.0)) % TwoPI
        }

        for di in 0...1 {
            for dj in 0...1 {
                let angle = orient.at((x + phy * (di + imax), y + phy * (dj + jmax)))
                if fieldSet {
                    field = averageAngle(field, angle)
                } else {
                    fieldSet = true
                    field = angle
                }
            }
        }

        return field
    }
}

func angleShader(#phy: Int, #source: Image) -> Shader {
    let half = phy / 2

    return { (image, color, x, y) in

        let lightColor = source.at((x, y)) * 0.5 + 0.5

        let cx = x - (x % phy) + half
        let cy = y - (y % phy) + half
        let vx = Double(x - cx)
        let vy = Double(y - cy)
        let angle = atan2(vy, vx)
        let refl = atan2(-vy, -vx)
        let rad = sqrt(vx*vx + vy*vy) / Double(half)
        let value = image.at((cx, cy))

        if rad >= 1 {
            return lightColor
        }

        let thresh = (1.2 - rad) * 0.5

        if rad < 0.1 ||
           abs(angle - value) < thresh ||
           abs(refl - value) < thresh {
            return 0.0
        } else {
            return lightColor
        }
    }
}

func fillShader(#phy: Int, #image: Image) -> Shader {
    var vmin = Double.infinity
    var vmax = -Double.infinity
    for y in 0...image.height-1 {
        for x in 0...image.width-1 {
            let value = image.at((x, y))
            if value < vmin { vmin = value }
            if value > vmax { vmax = value }
        }
    }
    let factor = max(vmax, vmin)
    println("max: \(vmax), min: \(vmin)")

    let half = phy / 2
    return { (image, color, x, y) in
        let cx = x - (x % phy) + half
        let cy = y - (y % phy) + half
        return image.at((cx, cy)) / factor
    }
}
