import Cocoa

func enhancedGradientBasedPipe(image: Image, #phy: Int) -> [String: Image] {
    let sobel = exec(sobelShader(), image)
    let (orient, coh, length) = exec(orientCohShader(phy: phy, sobel: sobel), image)
    let field = exec(blockEstimationShader(phy: phy, orient: orient, coherence: coh), image)
    return [
        "field": exec(angleShader(phy: phy, source: image), field),
        "coherence": exec(fillShader(phy: phy, image: coh), coh),
        "length": exec(fillShader(phy: phy, image: length), length)
    ]
}

public class App {

    let timer = Timer()

    init() {
        let path = Process.arguments.count > 1 
                 ? Process.arguments[1]
                 : "./res/06_2_114_7.bmp"
        let imageOpt = Image(fromFile: path)

        if imageOpt == nil {
            println("Unable to open '\(path)'")
            return
        }

        let image = imageOpt!
        let phy = 15

        timer.start()

        let result = enhancedGradientBasedPipe(image, phy: phy)
        println("Processing: \(timer.lap()) secs")

        Windows.show(image,                title: "Source")
        Windows.show(result["coherence"]!, title: "Coherence")
        Windows.show(result["length"]!,    title: "Length")
        Windows.show(result["field"]!,     title: "Field")
        println("Drawing:    \(timer.lap()) secs")
    }
}
