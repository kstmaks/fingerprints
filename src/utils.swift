import Cocoa

class Windows {
    private static var windows = [String: NSWindow]()

    static func show(image: Image, title: String) {
        var window = NSWindow(
                contentRect: NSMakeRect(0, 0, CGFloat(image.width), CGFloat(image.height)),
                styleMask: NSTitledWindowMask, 
                backing: NSBackingStoreType.Buffered, 
                defer: false
                )

        window.cascadeTopLeftFromPoint(NSMakePoint(40, 40))
        window.title = title
        window.makeKeyAndOrderFront(nil)
        window.backgroundColor = NSColor(patternImage: image.genNSImage())

        windows[title] = window
    }
}

func gauss(sigma: Double, #x: Int, #y: Int) -> Double {
    let sigma2 = 2.0 * sigma * sigma
    let xd = Double(x)
    let yd = Double(y)
    return exp(-(xd*xd + yd*yd)/sigma2) / (sigma2 * 3.1415)
}

let PI = 3.1415
let TwoPI = 2.0 * PI

class Timer {
    private var startDate: NSTimeInterval?

    func start() {
        startDate = NSDate.timeIntervalSinceReferenceDate()
    }

    func lap() -> Double {
        return NSDate.timeIntervalSinceReferenceDate() - startDate!
    }
}
