import Cocoa

public typealias Shader       = (Image, Double, Int, Int) -> Double
public typealias Shader2      = (Image, Double, Int, Int) -> (Double, Double)
public typealias Shader3      = (Image, Double, Int, Int) -> (Double, Double, Double)

func exec(shader: Shader, image: Image) -> Image {
    let data: [Double] = map(enumerate(image.data)) { (indexValue) in 
        let (i, value) = indexValue
        let x = i % image.width
        let y = (i - x) / image.width
        return shader(image, value, x, y)
    }
    return Image(data: data, width: image.width, height: image.height)
}

func exec(shader: Shader2, image: Image) -> Image2 {
    let data: [(Double, Double)] = map(enumerate(image.data)) { (indexValue) in 
        let (i, value) = indexValue
        let x = i % image.width
        let y = (i - x) / image.width
        return shader(image, value, x, y)
    }

    let fst: [Double] = map(data) { $0.0 }
    let snd: [Double] = map(data) { $0.1 }

    return (
        Image(data: fst, width: image.width, height: image.height),
        Image(data: snd, width: image.width, height: image.height)
        )   
}

func exec(shader: Shader3, image: Image) -> Image3 {
    let data: [(Double, Double, Double)] = map(enumerate(image.data)) { (indexValue) in 
        let (i, value) = indexValue
        let x = i % image.width
        let y = (i - x) / image.width
        return shader(image, value, x, y)
    }

    let fst: [Double] = map(data) { $0.0 }
    let snd: [Double] = map(data) { $0.1 }
    let trd: [Double] = map(data) { $0.2 }

    return (
        Image(data: fst, width: image.width, height: image.height),
        Image(data: snd, width: image.width, height: image.height),
        Image(data: trd, width: image.width, height: image.height)
        )   
}
