import Cocoa

NSApplication.sharedApplication()
NSApp.setActivationPolicy(NSApplicationActivationPolicy.Regular)

let menubar = NSMenu.new()
let appMenuItem = NSMenuItem.new()
menubar.addItem(appMenuItem)

let appMenu = NSMenu.new()
let appName = NSProcessInfo.processInfo().processName
let quitTitle = "Quit \(appName)"
let quitMenuItem = NSMenuItem(title: quitTitle, action: "terminate:", keyEquivalent: "q")

appMenu.addItem(quitMenuItem)
appMenuItem.submenu = appMenu

App()

NSApp.activateIgnoringOtherApps(true)
NSApp.run()
